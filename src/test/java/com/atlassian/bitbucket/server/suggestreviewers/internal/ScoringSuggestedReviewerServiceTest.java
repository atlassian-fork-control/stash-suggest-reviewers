package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.server.suggestreviewers.PullRequestDetails;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewer;
import com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.ContributorSuggester;
import com.atlassian.bitbucket.server.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.bitbucket.server.suggestreviewers.spi.SimpleReason;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.util.MoreStreams;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageUtils;
import com.atlassian.plugin.PluginAccessor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ScoringSuggestedReviewerServiceTest {

    private static final String USER_NAME = "jbourne";
    private static final String USER_SLUG = USER_NAME;
    private static final String USER_DISPLAY_NAME = "JSON Bourne";

    @Mock
    AuthenticationContext authenticationContext;
    @Mock
    CommitService commitService;
    @Mock
    MergeBaseResolver mergeBaseResolver;
    @Mock
    PermissionService permissionService;
    @Mock
    PluginAccessor pluginAccessor;
    @Mock
    SuggestedReviewerMetadataProvider reviewerMetadataProvider;

    @InjectMocks
    ScoringSuggestedReviewerService suggestedReviewerService;

    @Mock
    ApplicationUser currentUser;

    @Mock
    Repository repository;

    PullRequestDetails pullRequestDetails;

    @Mock
    ContributorSuggester contributorSuggester;

    Commit fromCommit;
    Commit toCommit;

    @Before
    public void setup() {
        when(currentUser.getName()).thenReturn(USER_NAME);
        mockPermissions(currentUser);

        when(commitService.getCommitsBetween(any(), isA(PageRequest.class))).thenAnswer(invocation ->
                PageUtils.createPage(ImmutableList.of(mockCommit()), invocation.getArgument(1)));

        fromCommit = mockCommit();
        toCommit = mockCommit();
        pullRequestDetails = new PullRequestDetails.Builder()
                .fromCommit(fromCommit)
                .toCommit(toCommit)
                .build();

        when(contributorSuggester.suggestFor(any()))
                .thenReturn(ImmutableMap.of(currentUser, ImmutableList.of(new SimpleReason("My name.", 100))));

        when(pluginAccessor.getEnabledModulesByClass(eq(ReviewerSuggester.class)))
                .thenReturn(ImmutableList.of(contributorSuggester));
    }

    @Test
    public void testFiltersCurrentUser() throws Exception {
        UserRanking ranking = new UserRanking();
        ranking.addRanking(currentUser, ImmutableList.of(new SimpleReason("I'm JSON Bourne!", 1000)));

        List<SuggestedReviewer> suggestedReviewerList = suggestedReviewerService.toSuggestedReviewers(currentUser,
                repository, ranking, 1);

        assertTrue(suggestedReviewerList.isEmpty());
    }

    @Test
    public void testFiltersUnlicensedUser() throws Exception {
        ApplicationUser noPermission = mock(ApplicationUser.class);
        mockLicensedUserPermission(noPermission, false);
        mockRepoReadPermission(noPermission, true);
        UserRanking ranking = new UserRanking();
        ranking.addRanking(noPermission, ImmutableList.of(new SimpleReason("", 1000)));

        List<SuggestedReviewer> suggestedReviewerList = suggestedReviewerService.toSuggestedReviewers(currentUser,
                repository, ranking, 1);

        assertTrue(suggestedReviewerList.isEmpty());
    }

    @Test
    public void testFiltersUnpermittedUser() throws Exception {
        ApplicationUser noPermission = mock(ApplicationUser.class);
        mockLicensedUserPermission(noPermission, true);
        mockRepoReadPermission(noPermission, false);
        UserRanking ranking = new UserRanking();
        ranking.addRanking(noPermission, ImmutableList.of(new SimpleReason("", 1000)));

        List<SuggestedReviewer> suggestedReviewerList = suggestedReviewerService.toSuggestedReviewers(currentUser,
                repository, ranking, 1);

        assertTrue(suggestedReviewerList.isEmpty());
    }

    @Test
    public void testRespectsLimit() throws Exception {
        int LIMIT = 1;
        UserRanking ranking = new UserRanking();
        ApplicationUser user1 = mock(ApplicationUser.class);
        mockPermissions(user1);
        ranking.addRanking(user1, ImmutableList.of(new SimpleReason("", 1000)));

        ApplicationUser user2 = mock(ApplicationUser.class);
        mockPermissions(user2);
        ranking.addRanking(user2, ImmutableList.of(new SimpleReason("", 1000)));

        assertTrue(ranking.getSortedRankedUserStream().count() > LIMIT);

        List<SuggestedReviewer> suggestedReviewerList = suggestedReviewerService.toSuggestedReviewers(currentUser,
                repository, ranking, LIMIT);

        assertEquals(LIMIT, suggestedReviewerList.size());
    }

    @Test
    public void testReturnsSuggestedReviewer() throws Exception {
        String REASON = "good reason";
        Integer REVIEWING = 23;
        UserRanking ranking = new UserRanking();
        ApplicationUser user = mock(ApplicationUser.class);
        mockPermissions(user);
        mockReviewingCount(user, REVIEWING);
        ranking.addRanking(user, ImmutableList.of(new SimpleReason(REASON, 1000)));

        List<SuggestedReviewer> suggestedReviewerList = suggestedReviewerService.toSuggestedReviewers(currentUser,
                repository, ranking, 1);

        assertEquals(1, suggestedReviewerList.size());
        SuggestedReviewer reviewer = suggestedReviewerList.get(0);
        assertEquals(user, reviewer.getUser());
        assertEquals(REASON, reviewer.getShortReason());
        assertEquals(REVIEWING, reviewer.getReviewingCount());
    }

    @Test
    public void testSuggestedReviewersSorted() throws Exception {
        String REASON = "good reason";
        UserRanking ranking = new UserRanking();
        ApplicationUser user = mock(ApplicationUser.class);
        mockPermissions(user);
        ranking.addRanking(user, ImmutableList.of(new SimpleReason(REASON, 1)));

        ApplicationUser user2 = mock(ApplicationUser.class);
        mockPermissions(user2);
        ranking.addRanking(user2, ImmutableList.of(new SimpleReason(REASON, 1000)));

        List<SuggestedReviewer> suggestedReviewerList = suggestedReviewerService.toSuggestedReviewers(currentUser,
                repository, ranking, 2);

        assertEquals(2, suggestedReviewerList.size());

        assertEquals(user2, suggestedReviewerList.get(0).getUser());
        assertEquals(user, suggestedReviewerList.get(1).getUser());
    }

    @Test
    public void testAlreadyMergedReturnsEmptyList() throws Exception {
        when(commitService.getCommitsBetween(any(), any())).thenAnswer(invocation ->
                PageUtils.createEmptyPage(invocation.getArgument(1)));

        assertNoReviewers();
    }

    @Test
    public void testNoRepoReturnsEmptyList() throws Exception {
        Commit fromCommit = mockCommit();
        when(fromCommit.getRepository()).thenReturn(null);
        pullRequestDetails = new PullRequestDetails.Builder()
                .fromCommit(fromCommit)
                .toCommit(toCommit)
                .build();

        assertNoReviewers();
    }

    @Test
    public void testSuggesterThrowingReturnsEmptyList() throws Exception {
        when(contributorSuggester.suggestFor(any())).thenThrow(new RuntimeException());

        assertNoReviewers();
    }

    @Test
    public void testInvalidSuggesterEntriesFiltered() throws Exception {
        when(contributorSuggester.suggestFor(any()))
                .thenReturn(ImmutableMap.of(currentUser, ImmutableList.of()));

        assertNoReviewers();
    }

    @Test
    public void testSuggestion() throws Exception {
        ApplicationUser user = mock(ApplicationUser.class);
        mockPermissions(user);
        String REASON = "User is life.";

        when(contributorSuggester.suggestFor(any()))
                .thenReturn(ImmutableMap.of(user, ImmutableList.of(new SimpleReason(REASON, 100))));

        Iterable<SuggestedReviewer> suggestedReviewers =
                suggestedReviewerService.getSuggestedReviewers(pullRequestDetails, 1);

        assertTrue(suggestedReviewers.iterator().hasNext());

        SuggestedReviewer reviewer = MoreStreams.streamIterable(suggestedReviewers).findFirst().get();
        assertEquals(user, reviewer.getUser());
        assertEquals(REASON, reviewer.getShortReason());
    }

    private void assertNoReviewers() {
        assertFalse(suggestedReviewerService.getSuggestedReviewers(pullRequestDetails, 100).iterator().hasNext());
    }

    private void mockLicensedUserPermission(ApplicationUser userMock, boolean hasPermission) {
        when(permissionService.hasGlobalPermission(eq(userMock), eq(Permission.LICENSED_USER))).thenReturn(hasPermission);
    }

    private void mockRepoReadPermission(ApplicationUser userMock, boolean hasPermission) {
        when(permissionService.hasRepositoryPermission(eq(userMock), eq(repository), eq(Permission.REPO_READ)))
                .thenReturn(hasPermission);
    }

    private void mockPermissions(ApplicationUser userMock) {
        mockLicensedUserPermission(userMock, true);
        mockRepoReadPermission(userMock, true);
    }

    private void mockReviewingCount(ApplicationUser userMock, int count) {
        when(reviewerMetadataProvider.getTotalReviewingCount(eq(userMock))).thenReturn(count);
    }

    private Commit mockCommit() {
        Commit commit = mock(Commit.class);
        when(commit.getId()).thenReturn(UUID.randomUUID().toString());
        when(commit.getRepository()).thenReturn(repository);
        return commit;
    }
}