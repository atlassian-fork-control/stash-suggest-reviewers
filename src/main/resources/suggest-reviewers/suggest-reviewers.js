define('suggested-reviewers', [
    'jquery',
    'lodash',
    'bitbucket/util/events',
    'exports'
], function(
    $,
    _,
    events,
    exports
) {

    var SuggestedReviewers = function(el, options) {
        _.bindAll(this, _.functions(this.constructor.prototype));
        this.$el = $(el);
        this.options = _.defaults({}, options, {
            suggestionLimit: 20,
            displayLimit: 4,
            avatarSize: 64
        });

        this.data = {
            selected: {},
            suggesting: {}
        };

        this.$el.on('click', '.add-suggested-reviewer', this.addReviewer);
        // 5.x (note: these events are not supported for non-Atlassian plugins to use as it could change in a minor release)
        events.on('bitbucket.internal.model.page-state.changed.sourceBranch', this.refresh);
        events.on('bitbucket.internal.model.page-state.changed.targetBranch', this.refresh);
        // 6.x (note: these events are not supported for non-Atlassian plugins to use as it could change in a minor release)
        events.on('bitbucket.internal.DO_NOT_USE.feature.repository.revisionReferenceSelector.revisionRefChanged', this.refresh);

        var self = this;
        $('.add-suggested-reviewer').tooltip({
            title: function() {
                var username = $(this).attr('data-name');
                var suggestion = self.data.suggesting[username];
                return AJS.escapeHtml(suggestion.shortReason) + '<br/>' +
                    AJS.I18n.getText('bitbucket.suggestedreviewers.reviewer.pull.requests',
                        suggestion.reviewingCount);
            },
            live: true,
            html: true
        });

        this.refresh();
    };

    SuggestedReviewers.prototype.refresh = function () {
        this.updateRepoInfo();
        this.show();
    };

    SuggestedReviewers.prototype.updateRepoInfo = function() {
        var fromRepo = $('input[name=fromRepoId]').val();
        var fromBranch = $('input[name=fromBranch]').val();

        var toRepo = $('input[name=toRepoId]').val();
        var toBranch = $('input[name=toBranch]').val();

        this.repoInfo = {
            fromRepoId: fromRepo,
            from: fromBranch,
            toRepoId: toRepo,
            to: toBranch
        };
    };

    SuggestedReviewers.prototype.fetch = function() {
        return $.ajax(AJS.contextPath() + '/rest/suggest-reviewers/1.0/by/ref', {
            data: _.assign({}, this.repoInfo, {
                count: this.options.suggestionLimit,
                avatarSize: this.options.avatarSize
            })
        });
    };

    SuggestedReviewers.prototype.onSuccess = function(data) {
        // filter out anyone already added to the review
        var selected = $('#reviewers').val().split('|!|');
        var self = this;
        self.data = {
            selected: {},
            suggesting: {}
        };
        _.forEach(data, function(suggestion) {
            if (selected.indexOf(suggestion.user.name) !== -1) {
                self.data.selected[suggestion.user.name] = suggestion;
            } else {
                self.data.suggesting[suggestion.user.name] = suggestion;
            }
        });

        this.render();
    };

    SuggestedReviewers.prototype.onFail = function(xhr, status, errorThrown) {
        var errorMsg = status + ' ' + errorThrown;
        this.$el.text(AJS.I18n.getText('bitbucket.suggestedreviewers.failed.to.load', errorMsg));
    };

    SuggestedReviewers.prototype.render = function () {
        var suggestions = _.values(this.data.suggesting);

        this.$el.html(bitbucket.server.suggest.reviewers.oneLine({
            suggestedReviewers: suggestions.slice(0, Math.min(this.options.displayLimit, suggestions.length))
        }));
    };

    SuggestedReviewers.prototype.renderLoading = function() {
        var suggestionsStr = AJS.I18n.getText('bitbucket.suggestedreviewers.suggestions');
        this.$el.html('<span>' + suggestionsStr + ' </span><div class="description-spinner"></div>');
        this.$el.find('.description-spinner').spin('small');
    };

    SuggestedReviewers.prototype.show = function() {
        this.renderLoading();

        if (this.repoInfo.from) { // The from ref defaults to empty when creating a PR through the UI
            this.fetch()
                .done(this.onSuccess)
                .fail(this.onFail);
        }
    };

    SuggestedReviewers.prototype.addReviewer = function(event) {
        var $reviewerSuggestion = $(event.target);
        var self = this;
        var username = $reviewerSuggestion.data('name');

        var select2 = $('.field-group.pull-request-reviewers #reviewers').data('select2');
        var reviewers = select2.data();

        //check we don't already exist
        if (!_.some(reviewers, function(r) {return r.id === username})) {
            self.data.selected[username] = self.data.suggesting[username];
            delete self.data.suggesting[username];
            reviewers.push({
                id: username,
                item: self.data.selected[username].user,
                text: username
            });
            select2.data(reviewers);
        }

        $reviewerSuggestion.next('span.comma').remove();
        $reviewerSuggestion.tipsy('hide');
        $reviewerSuggestion.remove();

        self.render();
    };


    /**
     * Initialize the suggested reviewers component.
     * @param selector the CSS selector of the content element for this component
     */
    exports.init = function(selector) {
        new SuggestedReviewers($(selector), {});
    };

});

jQuery(function() {
    require('suggested-reviewers').init('.pull-request-reviewers div.description');
});