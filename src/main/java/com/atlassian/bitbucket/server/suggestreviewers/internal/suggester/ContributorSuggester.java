package com.atlassian.bitbucket.server.suggestreviewers.internal.suggester;

import com.atlassian.bitbucket.commit.*;
import com.atlassian.bitbucket.server.suggestreviewers.PullRequestDetails;
import com.atlassian.bitbucket.server.suggestreviewers.internal.util.IntHolder;
import com.atlassian.bitbucket.server.suggestreviewers.internal.util.StringUtils;
import com.atlassian.bitbucket.server.suggestreviewers.spi.Reason;
import com.atlassian.bitbucket.server.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.bitbucket.server.suggestreviewers.spi.SimpleReason;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.Person;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ContributorSuggester implements ReviewerSuggester {

    private static final int SCORE_PER_COMMIT = 1000;
    private static final int MAX_COMMITS = 100;

    private final CommitService commitService;

    public ContributorSuggester(CommitService commitService) {
        this.commitService = commitService;
    }

    @Override
    public Map<ApplicationUser, Collection<Reason>> suggestFor(PullRequestDetails pullRequestDetails) {
        Commit since = pullRequestDetails.getMergeBase();
        Commit until = pullRequestDetails.getFromCommit();
        if (since == null || until.getRepository() == null) {
            return Collections.emptyMap();
        }
        CommitsBetweenRequest request = new CommitsBetweenRequest.Builder(until.getRepository())
            .include(until.getId())
            .exclude(since.getId())
            .secondaryRepository(until.getRepository())
            .build();

        IntHolder commitCounter = new IntHolder();
        Map<ApplicationUser, IntHolder> contributors = new HashMap<>();
        commitService.streamCommitsBetween(request, new AbstractCommitCallback() {

            @Override
            public boolean onCommit(@Nonnull Commit commit) {
                Person author = commit.getAuthor();
                if (author instanceof ApplicationUser) {
                    ApplicationUser user = (ApplicationUser) author;

                    IntHolder count = contributors.get(user);
                    if (count == null) {
                        count = new IntHolder();
                        contributors.put(user, count);
                    }
                    count.increment();
                }

                return commitCounter.increment() < MAX_COMMITS;
            }
        });

        Multimap<ApplicationUser, Reason> suggestions = HashMultimap.create();
        for (Map.Entry<ApplicationUser, IntHolder> entry : contributors.entrySet()) {
            int commitCount = entry.getValue().get();
            String description = commitCounter.get() < MAX_COMMITS ?
                    "Authored " + commitCount + " " + StringUtils.pluralize(commitCount, "commit", "commits") + " to be merged." :
                    "Authored some of the commits to be merged.";
            suggestions.put(entry.getKey(), new SimpleReason(description, commitCount * SCORE_PER_COMMIT));
        }
        return suggestions.asMap();
    }
}
