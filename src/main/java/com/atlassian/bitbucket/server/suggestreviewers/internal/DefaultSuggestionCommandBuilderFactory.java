package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderFactory;
import com.atlassian.bitbucket.scm.git.command.GitCommandBuilderSupport;
import com.atlassian.bitbucket.scm.git.command.GitScmCommandBuilder;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 3.1.1
 */
public class DefaultSuggestionCommandBuilderFactory implements SuggestionCommandBuilderFactory {

    private final GitCommandBuilderFactory builderFactory;

    public DefaultSuggestionCommandBuilderFactory(GitCommandBuilderFactory builderFactory) {
        this.builderFactory = builderFactory;
    }

    @Nonnull
    @Override
    @SuppressWarnings("ConstantConditions")
    public GitScmCommandBuilder builderFor(@Nonnull Commit since, @Nonnull Commit until) {
        return builderFor(requireNonNull(since, "since").getRepository(),
                requireNonNull(until, "until").getRepository());
    }

    @Nonnull
    @Override
    public GitScmCommandBuilder builderFor(@Nonnull Repository repository, @Nonnull Repository secondary) {
        GitScmCommandBuilder builder = builderFactory.builder(requireNonNull(repository, "repository"));
        // The cast here is for 6.x compatibility. Without this, this will be compiled to reference
        // GitScmCommandBuilder.alternates(Iterable), which was removed in 6.0. By casting to
        // GitCommandBuilderSupport we get a compiled reference to a method that exists in both 5.x and 6.x
        ((GitCommandBuilderSupport) builder).alternates(ImmutableList.of(requireNonNull(secondary, "secondary")));

        return builder;
    }
}
