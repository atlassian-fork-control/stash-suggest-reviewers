package com.atlassian.bitbucket.server.suggestreviewers.spi;

import java.util.Objects;

/**
 * A convenient implementation of {@link Reason}.
 *
 * @since 1.0
 */
public class SimpleReason implements Reason {

    private final String shortDescription;
    private final String description;
    private final int score;

    public SimpleReason(String description, int score) {
        this(description, description, score);
    }

    public SimpleReason(String shortDescription, String description, int score) {
        this.shortDescription = shortDescription;
        this.description = description;
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof SimpleReason)) {
            return false;
        }
        return compareTo((SimpleReason) o) == 0;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getShortDescription() {
        return shortDescription;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public int compareTo(Reason o) {
        return score - o.getScore();
    }

    @Override
    public int hashCode() {
        return Objects.hash(shortDescription, description, score);
    }
}
