package com.atlassian.bitbucket.server.suggestreviewers.internal.util;

public final class StringUtils {

    public static String pluralize(int amount, String single, String plural) {
        return amount == 1 ? single : plural;
    }

}
