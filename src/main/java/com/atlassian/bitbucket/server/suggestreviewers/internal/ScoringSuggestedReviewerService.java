package com.atlassian.bitbucket.server.suggestreviewers.internal;

import com.atlassian.bitbucket.auth.AuthenticationContext;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.server.suggestreviewers.PullRequestDetails;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewer;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewerService;
import com.atlassian.bitbucket.server.suggestreviewers.spi.Reason;
import com.atlassian.bitbucket.server.suggestreviewers.spi.ReviewerSuggester;
import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.ApplicationUserEquality;
import com.atlassian.bitbucket.util.PageUtils;
import com.atlassian.plugin.PluginAccessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

public class ScoringSuggestedReviewerService implements SuggestedReviewerService {

    private static final Logger log = LoggerFactory.getLogger(ScoringSuggestedReviewerService.class);

    private final AuthenticationContext authenticationContext;
    private final CommitService commitService;
    private final MergeBaseResolver mergeBaseResolver;
    private final PermissionService permissionService;
    private final PluginAccessor pluginAccessor;
    private final SuggestedReviewerMetadataProvider reviewerMetadataProvider;

    public ScoringSuggestedReviewerService(AuthenticationContext authenticationContext,
                                           CommitService commitService, MergeBaseResolver mergeBaseResolver,
                                           PermissionService permissionService, PluginAccessor pluginAccessor,
                                           SuggestedReviewerMetadataProvider reviewerMetadataProvider) {
        this.authenticationContext = authenticationContext;
        this.commitService = commitService;
        this.mergeBaseResolver = mergeBaseResolver;
        this.permissionService = permissionService;
        this.pluginAccessor = pluginAccessor;
        this.reviewerMetadataProvider = reviewerMetadataProvider;
    }

    @Override
    public Iterable<SuggestedReviewer> getSuggestedReviewers(PullRequestDetails details, int limit) {
        Repository fromRepo = details.getFromCommit().getRepository();
        Repository toRepo = details.getToCommit().getRepository();
        if (fromRepo == null || toRepo == null) {
            return emptyList();
        }
        if (isAlreadyMerged(toRepo, details.getToCommit(), fromRepo, details.getFromCommit())) {
            return emptyList();
        }

        PullRequestDetails pullRequestDetails = ensureValidDetails(details);

        UserRanking ranking = new UserRanking();
        pluginAccessor.getEnabledModulesByClass(ReviewerSuggester.class).stream()
                .map(suggester -> maybeGetSuggestions(suggester, pullRequestDetails))
                .filter(Objects::nonNull)
                .filter(map -> !map.isEmpty())
                .map(Map::entrySet)
                .flatMap(Set::stream)
                .filter(entry -> !(entry.getKey() == null || entry.getValue() == null || entry.getValue().isEmpty()))
                .forEach(entry -> ranking.addRanking(entry.getKey(), entry.getValue()));

        return toSuggestedReviewers(authenticationContext.getCurrentUser(), fromRepo, ranking, limit);
    }

    List<SuggestedReviewer> toSuggestedReviewers(ApplicationUser currentUser, Repository repository,
                                                 UserRanking ranking, int limit) {
        return ranking.getSortedRankedUserStream()
                // Don't suggest the authenticated user; they can't review their own work
                .filter(rankedUser -> !(currentUser != null && ApplicationUserEquality.equals(currentUser, rankedUser.getUser())))
                // Don't suggest unlicensed users
                .filter(rankedUser -> permissionService.hasGlobalPermission(rankedUser.getUser(), Permission.LICENSED_USER))
                // Don't suggest users without REPO_READ permission
                .filter(rankedUser -> permissionService.hasRepositoryPermission(rankedUser.getUser(), repository, Permission.REPO_READ))
                .limit(limit)
                .map(rankedUser -> {
                    ApplicationUser user = rankedUser.getUser();
                    return new SimpleSuggestedReviewer(user, ranking.getMostRelevantReason(user).getShortDescription(),
                            ranking.getAllReasons(user).stream().map(Reason::getDescription).collect(toList()),
                            reviewerMetadataProvider.getTotalReviewingCount(user));

                }).collect(toList());
    }

    private static Map<ApplicationUser, Collection<Reason>> maybeGetSuggestions(ReviewerSuggester suggester,
                                                                                PullRequestDetails details) {
        try {
            return suggester.suggestFor(details);
        } catch (Exception e) {
            log.error("{} threw an exception (or returned unexpected data) when" +
                      " suggesting reviewers and was ignored.", suggester.getClass(), e);
            return null;
        }
    }

    private PullRequestDetails ensureValidDetails(PullRequestDetails details) {
        if (details.getMergeBase() == null) {
            Commit mergeBase = mergeBaseResolver.findMergeBase(details.getFromCommit(), details.getToCommit());

            return new PullRequestDetails.Builder()
                    .fromCommit(details.getFromCommit())
                    .toCommit(details.getToCommit())
                    .fromRefId(details.getFromRefId())
                    .toRefId(details.getFromRefId())
                    .mergeBase(mergeBase)
                    .build();
        }

        return details;
    }

    private boolean isAlreadyMerged(Repository toRepo, Commit since, Repository fromRepo, Commit until) {
        CommitsBetweenRequest request = new CommitsBetweenRequest.Builder(toRepo)
                .include(until.getId())
                .exclude(since.getId())
                .secondaryRepository(fromRepo)
                .build();
        return commitService.getCommitsBetween(request, PageUtils.newRequest(0, 1)).getSize() == 0;
    }

}