package com.atlassian.bitbucket.server.suggestreviewers.internal.rest;

import com.atlassian.bitbucket.user.ApplicationUser;
import com.atlassian.bitbucket.user.UserService;
import com.atlassian.bitbucket.server.suggestreviewers.spi.UserResolver;

import java.util.ArrayList;
import java.util.List;

public class UserServiceUserResolver implements UserResolver {

    private final UserService userService;

    public UserServiceUserResolver(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Iterable<ApplicationUser> resolve(Iterable<String> emailAddresses) {
        List<ApplicationUser> resolvedUsers = new ArrayList<>();

        for (String email : emailAddresses) {
            ApplicationUser user = resolve(email);
            if (user != null) {
                resolvedUsers.add(user);
            }
        }

        return resolvedUsers;
    }

    @Override
    public ApplicationUser resolve(String emailAddress) {
        return userService.findUserByNameOrEmail(emailAddress);
    }
}
