package com.atlassian.bitbucket.server.suggestreviewers.spi;

import com.atlassian.bitbucket.server.suggestreviewers.PullRequestDetails;
import com.atlassian.bitbucket.user.ApplicationUser;

import java.util.Collection;
import java.util.Map;

/**
 * A plugin module that suggests appropriate reviewers for a set of changes.
 *
 * @since 1.0
 */
public interface ReviewerSuggester {

    /**
     * @param pullRequestDetails the details of the pull request to suggest reviewers for
     * @return a multimap of {@link ApplicationUser users} who would be appropriate reviewers for the specified
     *         changes, and a the corresponding {@link Reason reasons} they are being suggested.
     */
    Map<ApplicationUser, Collection<Reason>> suggestFor(PullRequestDetails pullRequestDetails);

}
